// @author Mitchel Morash
// This Game started as a project for PROG1300

let x2 = -10;
let y2 = 25;
let x3 = 10;
let y3 = 25;
let x2plot = 390;
let y2plot = 425;
let x3plot = 410;
let y3plot = 425;
let xOrig;
let yOrig;
let xChange;
let yChange;
let x4;
let y4;


/**
 * A triangle class, used to make the ship.
 * It inherits from Shape.
 */
class Triangle extends Shape {

    /**
     * A triangle constructor.
     * @param xPos : int for X position of Triangle.
     * @param yPos : int for Y position of Triangle.
     * @param xVel : int for X velocity of Triangle.
     * @param yVel : int for Y velocity of triangle.
     * @param size : int for size of triangle.
     * @param name : string for name of triangle.
     */
	constructor(xPos, yPos, xVel, yVel, size, name) {
		super(xPos, yPos, xVel, yVel, size, name);
		xOrig = this.xpos;
		yOrig = this.ypos;
	}
}

/**
 * This function is used to draw the Triangle on the canvas.
 * @param ctx : CanvasRenderingContext2D canvas context to draw on.
 * @param safe : is a boolean to tell us to color the ship green for being safe from collision.
 */
Triangle.prototype.draw = function (ctx, safe) {
	
	ctx.beginPath();
	ctx.moveTo(xOrig, yOrig);
	ctx.lineTo(x2plot, y2plot);
	ctx.lineTo(x3plot, y3plot);
	ctx.closePath();
	
	ctx.lineWidth = 2;
	ctx.strokeStyle = 'white';
	if (safe) {
		ctx.strokeStyle = 'green';
	}
	ctx.stroke();
};

/**
 * This function is used to rotate the ship left.
 */
Triangle.prototype.leftRotate = function () {
	const right = 0.05;
	this.rotate(right);
};

/**
 * This function is used to rotate the ship right.
 */
Triangle.prototype.rightRotate = function () {
	const left = -0.05;
	this.rotate(left);
};

/**
 * This function is used to rotate the ship based on value passed in.
 * @param rotate : int for the amount to rotate.
 */
Triangle.prototype.rotate = function (rotate) {

	let temp = (x2*(Math.cos(rotate)))+(y2*(Math.sin(rotate)));
	let temp2 = (-x2*(Math.sin(rotate)))+(y2*(Math.cos(rotate)));
	let temp3 = (x3*(Math.cos(rotate)))+(y3*(Math.sin(rotate)));
	let temp4 = (-x3*(Math.sin(rotate)))+(y3*(Math.cos(rotate)));

	x2plot = temp + xOrig;
	y2plot = temp2 + yOrig;
	x3plot = temp3 + xOrig;
	y3plot = temp4 + yOrig;

	x2 = temp;
	y2 = temp2;
	x3 = temp3;
	y3 = temp4;
};

/**
 * This function is used to get the change in X for movement.
 * @returns {number} : int the change in X.
 */
function getChangeX() {
	x4 = (x3 + x2)/2;
	y4 = (y3 + y2)/2;

	return xChange = (parseInt(x4)/parseInt(y4));
}

/**
 * This function is used to get the change in Y for movement.
 * @returns {number} : int the change in Y.
 */
function getChangeY() {
	x4 = (x3 + x2)/2;
	y4 = (y3 + y2)/2;

	return yChange = (parseInt(y4)/parseInt(x4));
}

/**
 * This function is used to move the ship forward.
 */
Triangle.prototype.moveForward = function () {
	this.move(-getChangeY(), -getChangeX());
};

/**
 * This function is used to move the ship backwards.
 */
Triangle.prototype.moveBackward = function () {
	this.move(getChangeY(), getChangeX());
};

/**
 * This function is used to move the ship.
 * @param yChange : int for the change in Y position.
 * @param xChange : int for the change in X position.
 */
Triangle.prototype.move = function (yChange, xChange) {

	if (xChange > 2) {
		xChange = 2;
	} else if (xChange < -2) {
		xChange = -2;
	} else if (yChange > 2) {
		yChange = 2;
	} else if (yChange < -2) {
		yChange = -2;
	}

	if (x4 === 0 && y4 >= 0) {
		//console.log("up");
		yOrig += yChange;
		y2plot += yChange;
		y3plot += yChange;
	}else if (x4 === 0 && y4 <= 0){
		//console.log("down");
		yOrig -= yChange;
		y2plot -= yChange;
		y3plot -= yChange;
	}else if (x4 >= 0 && y4 === 0){
		//console.log("left");
		xOrig += xChange;
		x2plot += xChange;
		x3plot += xChange;
	}else if (x4 <= 0 && y4 === 0){
		//console.log("right");
		xOrig -= xChange;
		x2plot -= xChange;
		x3plot -= xChange;
	}else if (x4 <= 0 && y4 <= 0) {
		//console.log("down right");
		xOrig -= xChange;
		yOrig -= yChange;
		x2plot -= xChange;
		y2plot -= yChange;
		x3plot -= xChange;
		y3plot -= yChange;
	}else if (x4 >= 0 && y4 <= 0) {
		//console.log("down left");
		xOrig -= xChange;
		yOrig += yChange;
		x2plot -= xChange;
		y2plot += yChange;
		x3plot -= xChange;
		y3plot += yChange;
	}else if (x4 >= 0 && y4 >= 0) {
		//console.log("up left");
		xOrig += xChange;
		yOrig += yChange;
		x2plot += xChange;
		y2plot += yChange;
		x3plot += xChange;
		y3plot += yChange;
	}else if (x4 <= 0 && y4 >= 0) {
		//console.log("up right");
		xOrig += xChange;
		yOrig -= yChange;
		x2plot += xChange;
		y2plot -= yChange;
		x3plot += xChange;
		y3plot -= yChange;
	}

	if (this.checkX1()<0){
		//console.log("top out");
		xOrig = 775;
		x2plot = 800;
		x3plot = 800;
	} else if (this.checkX1()>800) {
		//console.log("bottom out");
		xOrig = 25;
		x2plot = 0;
		x3plot = 0;
	} else if (this.checkY1()<0) {
		//console.log("left out");
		yOrig = 775;
		y2plot = 800;
		y3plot = 800;
	}else if (this.checkY1()>800) {
		//console.log("right out");
		yOrig = 25;
		y2plot = 0;
		y3plot = 0;
	}
};

/**
 * This function is used to set the bottom left & bottom right
 * points back to their original positions.
 */
Triangle.prototype.startPos = function () {
	// sets ship to original position
	x2plot = 390;
	y2plot = 425;
	x3plot = 410;
	y3plot = 425;

	xChange = 0;
	yChange = 2;

	x2 = -10;
	y2 = 25;
	x3 = 10;
	y3 = 25;
};

/**
 * This function is used to get the bottom left Y position.
 * @returns {number} : is the bottom left Y position.
 */
Triangle.prototype.checkY2 = function () {
	return y2plot;
};

/**
 * This function is used to get the bottom left X position
 * @returns {number} : is the bottom left X position.
 */
Triangle.prototype.checkX2 = function () {
	return x2plot;
};

/**
 * This function is used to get the bottom right Y position
 * @returns {number} : is the bottom right Y position.
 */
Triangle.prototype.checkY3 = function () {
	return y3plot;
};

/**
 * This function is used to get the bottom right X position
 * @returns {number} : is the bottom right X position.
 */
Triangle.prototype.checkX3 = function () {
	return x3plot;
};

/**
 * This function is used to get the top X position
 * @returns {number} : is the bottom top X position.
 */
Triangle.prototype.checkX1 = function () {
	return xOrig;
};

/**
 * This function is used to get the top Y position
 * @returns {number} : is the bottom top Y position.
 */
Triangle.prototype.checkY1 = function () {
	return yOrig;
};
