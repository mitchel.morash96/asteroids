README doc for Asteroids game
@author Mitchel Morash

This Game was done for a project in PROG1300

@Controls
           W = Move forward
           A = Rotate left
           S = Move backward
           D = Rotate right
    SPACEBAR = Shoot

If you find a "BUG" please report it to me @ w0243164@campus.nscc.ca