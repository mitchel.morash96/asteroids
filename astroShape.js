// @author Mitchel Morash
// This Game started as a project for PROG1300

/**
 * A shape class.
 * To be inherited by from other shapes
 */
class Shape {

    /**
     * A constructor for a shape object.
     * @param xPos : int for X position of object
     * @param yPos : int for Y position of object
     * @param xVel : int for X velocity of object
     * @param yVel : int for Y velocity of object
     * @param size : int for size of the object
     * @param name : string for name of the object
     */
	constructor(xPos, yPos, xVel, yVel, size, name) {
		this.xpos = xPos;
		this.ypos = yPos;
		this.name = name;
		this.size = size;
		this.xvel = xVel;
		this.yvel = yVel;
	}
}



