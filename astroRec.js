// @author Mitchel Morash
// This Game started as a project for PROG1300

/**
 * A rectangle Shape, used to make missiles.
 * It inherits from Shape.
 */
class Rectangle extends Shape {

	/**
	 * A constructor for rectangle.
	 * @param xPos : int for X position of front of Rectangle.
	 * @param yPos : int for Y position of front of Rectangle.
	 * @param xVel : int for X velocity of Rectangle.
	 * @param yVel : int for Y velocity of Rectangle.
	 * @param size : int for size of Rectangle.
	 * @param name : string for name of Rectangle.
	 * @param xPos2 : int for X position of back of Rectangle.
	 * @param yPos2 : int for Y position of back of Rectangle.
	 */
	constructor(xPos, yPos, xVel, yVel, size, name, xPos2, yPos2) {
		super(xPos, yPos, xVel, yVel, size, name);
		this.xpos2 = xPos2;
		this.ypos2 = yPos2;
	}
}

/**
 * This function is used to draw the rectangle on the canvas.
 * @param ctx : CanvasRenderingContext2D canvas context to draw on.
 */
Rectangle.prototype.draw = function (ctx) {
    ctx.beginPath();
	ctx.lineWidth = 6;
	ctx.moveTo(this.xpos, this.ypos);
	ctx.lineTo(this.xpos2, this.ypos2);
	ctx.strokeStyle = "white";
	ctx.stroke();
};

/**
 * This function is used to move the missile around the canvas.
 */
Rectangle.prototype.move = function () {
	this.xpos2 += this.xvel;
	this.ypos2 += this.yvel;
	this.xpos += this.xvel;
	this.ypos += this.yvel;
};

/**
 * This function returns the Y position of the front of the Rectangle.
 * @returns Y : The Y position on the canvas.
 */
Rectangle.prototype.checkY = function () {
	return this.ypos;
};

/**
 * This function returns the X position of the front of the Rectangle.
 * @returns X : The X position on the canvas.
 */
Rectangle.prototype.checkX = function () {
	return this.xpos;
};
