// @author Mitchel Morash
// This Game started as a project for PROG1300

let canvas;
let ctx;
let startBtn;
let resetBtn;
let asteroidCount = 0;
const asteroidSizes = [15, 30, 45, 60];
const keyCode_Left = "a";
const keyCount_Right = "d";
const keyCount_Up = "w";
const keyCount_Down = "s";
const keyCode_SpaceBar = " ";
let keyboardMoveLeft = false;
let keyboardMoveRight = false;
let keyboardMoveUp = false;
let keyboardMoveDown = false;
let mX, mY, sX, sY;
let sX2, sY2, sX3, sY3;
let aX, aY;
let rad;
let timer;
let count = 0;
let counter = 0;
let score = 0;
let lives = 3;
let highScore;
const FPS = 16;
let safe = false;
let dead = false;

let ship = new Triangle(400, 400, 0, 0, 10, 'Ship');

let asteroidArray = [];
let shipArray = [ship];
let missileArray = [];

/**
 * This function is used to initialize the game.
 * Also checks local storage for a previous high score.
 * @returns timer : is a variable which tells the canvas to refresh.
 */
function init() {
	
	if(typeof(Storage) !== "undefined") {
		if(localStorage.highScore===undefined) {
			localStorage.highScore = 0;
		}
		highScore = localStorage.highScore;
	}
	else {
		highScore = 0;
	}
	
    canvas = document.getElementById("Game");
    ctx = canvas.getContext("2d");
    timer = setInterval(draw_screen, FPS);
	window.onkeyup = keyUpHandler;
	window.onkeydown = keyDownHandler;
	draw_screen();

	startBtn = document.getElementById("start");
	resetBtn = document.getElementById("restart");
	startBtn.style.display = "none";

    console.log('init() done');
    return timer;
}

/**
 * This function is used to handle keyDown events.
 * @param e : KeyboardEvent used to get the key code for the event.
 */
function keyDownHandler(e) {
	switch(e.key) {
		case keyCode_Left:
			keyboardMoveLeft = true;
		break;
		case keyCount_Right:
			keyboardMoveRight = true;
		break;
		case keyCount_Up:
			keyboardMoveUp = true;
		break;
		case keyCount_Down:
			keyboardMoveDown = true;
		break;
	}
}

/**
 * This function is used to handle keyUp events.
 * @param e : KeyboardEvent used to get the key code for the event.
 */
function keyUpHandler(e) {
	switch(e.key) {
		case keyCode_Left:
			keyboardMoveLeft = false;
		break;
		case keyCount_Right:
			keyboardMoveRight = false;
		break;
		case keyCount_Up:
			keyboardMoveUp = false;
		break;
		case keyCount_Down:
			keyboardMoveDown = false;
		break;
		case keyCode_SpaceBar:
			createMissile();
		break;
	}
}

/**
 * This function is used to create an asteroid.
 * The asteroid will be created with one of 4 possible sizes,
 * and will created along one of the walls.
 */
function createAsteroids() {
	if (asteroidCount < 10) {
		for(let i = asteroidCount; i<=9; i++) {
			let asteroid;

			let temp = Math.floor(Math.random() * 4);
			let temp2 = Math.floor(Math.random() * 801);
			let temp3 = Math.floor(Math.random() * 4);
			let temp4 = (Math.random()*3)-1;
			let temp5 = (Math.random()*3)-1;

			if (temp3===0) {
				asteroid = new Circle(temp2, 50, temp4, temp5, asteroidSizes[temp], 'Asteroid');
			}else if (temp3===1) {
				asteroid = new Circle(750, temp2, temp4, temp5, asteroidSizes[temp], 'Asteroid');
			}else if (temp3===2) {
				asteroid = new Circle(temp2, 750, temp4, temp5, asteroidSizes[temp], 'Asteroid');
			}else if (temp3===3) {
				asteroid = new Circle(50, temp2, temp4, temp5, asteroidSizes[temp], 'Asteroid');
			}				
			asteroidArray.push(asteroid);
			asteroidCount++;
			console.log('asteroid made' + temp);
		}
	}		
}

/**
 * This function is used to destroy an asteroid.
 * Also if the Asteroid is of any size that is not the smallest,
 * it will break into two asteroids of a smaller size.
 * @param count : is the index of the asteroid in the asteroid array.
 */
function destroyAsteroid(count) {
	let temp = asteroidArray[count];
	asteroidArray.splice(count, 1);

	let newAsteroid;
	let newAsteroid2;

	let temp4 = (Math.random()*3)-1;
	let temp5 = (Math.random()*3)-1;

	let temp6 = (Math.random()*3)-1;
	let temp7 = (Math.random()*3)-1;

	let snd = new Audio("sound/bangLarge.wav");
	snd.volume = 0.1;
	snd.play().then(() => {
		//console.log("audio played!");
	});

	asteroidCount--;
	if (temp.size === 'undefined') {
		console.log("size is undefined");
	} else if (temp.size === 30) {
		console.log("small");
		newAsteroid = new Circle(temp.xpos, temp.ypos, temp4, temp5, asteroidSizes[0], 'Asteroid');
		newAsteroid2 = new Circle(temp.xpos, temp.ypos, temp6, temp7, asteroidSizes[0], 'Asteroid');
		asteroidArray.push(newAsteroid);
		asteroidArray.push(newAsteroid2);
		asteroidCount+=2;
		score += 200;
	} else if (temp.size === 45) {
		console.log("medium");
		newAsteroid = new Circle(temp.xpos, temp.ypos, temp4, temp5, asteroidSizes[1], 'Asteroid');
		newAsteroid2 = new Circle(temp.xpos, temp.ypos, temp6, temp7, asteroidSizes[1], 'Asteroid');
		asteroidArray.push(newAsteroid);
		asteroidArray.push(newAsteroid2);
		asteroidCount+=2;
		score += 300;
	} else if (temp.size === 60) {
		console.log("large");
		newAsteroid = new Circle(temp.xpos, temp.ypos, temp4, temp5, asteroidSizes[2], 'Asteroid');
		newAsteroid2 = new Circle(temp.xpos, temp.ypos, temp6, temp7, asteroidSizes[2], 'Asteroid');
		asteroidArray.push(newAsteroid);
		asteroidArray.push(newAsteroid2);
		asteroidCount+=2;
		score += 400;
	} else {
		score += 100;
	}
	//console.log(asteroidArray);
	draw_screen();
}

/**
 * This function is used to destroy a missile.
 * this is done by removing the object form the missile array.
 */
function destroyMissile() {
	missileArray.shift();
	console.log("missile destroy")
}

/**
 * This function is used to create a missile object.
 * It creates the missile by finding what way the ship is pointing,
 * and setting the path of the missile to that direction.
 */
function createMissile() {
	let x2;
	let y2;
	let x3;
	let y3;
	let x4;
	let y4;
	let newX;
	let newY;
	let shipOriginX;
	let shipOriginY;

	x2 = ship.checkX2();
	x3 = ship.checkX3();
	y2 = ship.checkY2();
	y3 = ship.checkY3();
	shipOriginX = ship.checkX1();
	shipOriginY = ship.checkY1();
	
	x4 = (x3 + x2)/2;
	y4 = (y3 + y2)/2;
	
	newX = shipOriginX - x4;
	newY = shipOriginY - y4;
	
	if (x4 > shipOriginX) {
	}
	else if (x4 < shipOriginX) {
	}
	else if (y4 > shipOriginY) {
		newY += 15;
	} 
	else if (y4 < shipOriginY) {
		newY += 15;
	}

	let snd = new Audio("sound/fire.wav");
	snd.volume = 0.1;
	snd.play().then(() => {
		//console.log("audio played!");
	});

	let missile = new Rectangle(shipOriginX, shipOriginY, newX, newY, 0, 'missile', x4, y4);
	missileArray.push(missile);
}

/**
 * This function is used to rotate the ship to the left.
 */
function rotateShipLeft() {
	shipArray.forEach(function (shape) {
		shape.leftRotate();
	});
}

/**
 * This function is used to rotate the ship to the right.
 */
function rotateShipRight() {
	shipArray.forEach(function (shape) {
		shape.rightRotate();
	});
}

/**
 * This function is used to move the ship forward.
 */
function moveForward() {
	shipArray.forEach(function (shape) {
		shape.moveForward();
	});
}

/**
 * This function is used to move the ship backwards.
 */
function moveBackwards() {
	shipArray.forEach(function (shape) {
		shape.moveBackward();
	});
}

/**
 * This function is used to update the canvas.
 */
function draw_screen() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "black";
	
	createAsteroids();
	
	ctx.font = "30px Arial";
	ctx.fillStyle = "white";
	ctx.fillText("HighScore: " + highScore,10,30);
	ctx.fillText("Score: " + score,10,55);
	ctx.fillText("Lives: " + lives,10,80);
	
	asteroidArray.forEach(function (shape) {
		shape.move();
	});
	
	asteroidArray.forEach(function (shape) {
		shape.draw(ctx);
	});
	
	shipArray.forEach(function (shape) {
		shape.draw(ctx, safe);
	});
	
	missileArray.forEach(function (shape) {
		let tempY = shape.checkY();
		let tempX = shape.checkX();
		if (tempY<0) {
			destroyMissile();
		} else if (tempY>800) {
			destroyMissile();
		} else if (tempX<0) {
			destroyMissile();
		} else if (tempX>800) {
			destroyMissile();
		}
	});
	
	missileArray.forEach(function (shape) {
		shape.draw(ctx);
	});
	
	missileArray.forEach(function (shape) {
		shape.move();
	});

	if (dead) {
		setGameOver();
	}
	checkCollision();
	
	if(keyboardMoveLeft) {
		//console.log("Keyboard - Left");
		rotateShipLeft();
	}
	if(keyboardMoveRight) {
		//console.log("Keyboard - Right");
		rotateShipRight();
	}
	if(keyboardMoveUp) {
		//console.log("Keyboard - Up");
		moveForward();
	}
	if(keyboardMoveDown) {
		//console.log("Keyboard - Down");
		moveBackwards();
	}
}

/**
 * This function is to check collision between objects.
 */
function checkCollision() {

	// collision between missiles and asteroids
	missileArray.forEach(function (shape) {
		mX = shape.checkX();
		mY = shape.checkY();

		asteroidArray.forEach(function (shape) {
			aX = shape.getX();
			aY = shape.getY();
			rad = shape.getSize();
			if (getDistance(mX, mY, aX, aY) < rad) {
				console.log("hit");
				destroyMissile();
				destroyAsteroid(count);
			}
			count += 1;
		});
		count = 0;
	});

	// collision between the ship and asteroids
	shipArray.forEach(function (shape) {
		sX = shape.checkX1();
		sY = shape.checkY1();
		sX2 = shape.checkX2();
		sY2 = shape.checkY2();
		sX3 = shape.checkX3();
		sY3 = shape.checkY3();

		asteroidArray.forEach(function (shape) {
			aX = shape.getX();
			aY = shape.getY();
			rad = shape.getSize();
			if (safe !== true) {
				if (getDistance(sX, sY, aX, aY) < rad) {
					console.log("hit");
					destroyAsteroid(count);
					lives -= 1;
					safe = true;
					console.log("Crash1");
				} else if (getDistance(sX2, sY2, aX, aY) < rad) {
					console.log("hit");
					destroyAsteroid(count);
					lives -= 1;
					safe = true;
					console.log("Crash2");
				} else if (getDistance(sX3, sY3, aX, aY) < rad) {
					console.log("hit");
					destroyAsteroid(count);
					lives -= 1;
					safe = true;
					console.log("Crash3");
				}
			}
			if (safe === true) {
				counter++;
				if (counter === 1000) {
					safe = false;
					counter = 0;
				}
			}
			if(lives <= 0) {
				console.log("exit");
				if(highScore<score) {
					highScore = score;
					localStorage.highScore = score;
				}
				dead = true;
				//clearTimeout(timer);
				resetBtn.style.display = "block";
			}
			count += 1;
		});
		count = 0;
	});
}

/**
 * This function ends the game loop.
 */
function setGameOver() {
	clearTimeout(timer);
}

/**
 * This function is used to find the distance between two points.
 * @param x1 : int for X position of the first point.
 * @param y1 : int for Y position of the first point.
 * @param x2 : int for X position of the second point.
 * @param y2 : int for Y position of the second point.
 * @returns {number} : The distance between two points.
 */
function getDistance(x1, y1, x2, y2) {
	let xDistance = x2 - x1;
	let yDistance = y2 - y1;
	
	return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
}

/**
 * is not implemented yet!
 * but will be used to reset game back to its default state.
 */
function reset() {
	asteroidArray = [];
	missileArray = [];
	shipArray = [];
	console.log("RESET");
	lives = 3;
	score = 0;
	asteroidCount = 0;
	ship = new Triangle(400, 400, 0, 0, 10, 'Ship');
	ship.startPos();
	shipArray.push(ship);
	timer = setInterval(draw_screen, FPS);
	safe = false;
	dead = false;
	resetBtn.style.display = "none";
}
