// @author Mitchel Morash
// This Game started as a project for PROG1300

/**
 * A Circle class, used to make asteroids.
 * It inherits from Shape.
 */
class Circle extends Shape {

    /**
     * A constructor for Circle objects.
     * @param xPos : int for X position of Circle
     * @param yPos : int for Y position of Circle
     * @param xVel : int for X velocity of Circle
     * @param yVel : int for Y velocity of Circle
     * @param size : int for size of Circle
     * @param name : string for name of Circle
     */
	constructor(xPos, yPos, xVel, yVel, size, name) {
		super(xPos, yPos, xVel, yVel, size, name)
	}
}

/**
 * This function draws the circle on the canvas.
 * @param ctx : CanvasRenderingContext2D canvas context to draw on.
 */
Circle.prototype.draw = function (ctx) {
    ctx.beginPath();
    ctx.arc(this.xpos, this.ypos, this.size, 0, Math.PI * 2, true);
	ctx.lineWidth = 2;
    ctx.strokeStyle = 'white';
    ctx.stroke();
};

/**
 * This function moves the circles around the canvas.
 * If the circle hits the edge of the canvas, it will appear on the other side.
 */
Circle.prototype.move = function () {

	this.xpos += this.xvel;
	this.ypos += this.yvel;

	if (this.xpos<0) {
		this.xpos = 800;
	} else if (this.xpos>800) {
		this.xpos = 0;
	} else if (this.ypos<0) {
		this.ypos = 800;
	}else if (this.ypos>800) {
		this.ypos = 0;
	}
};

/**
 * This function is used to return the X position of the Circle.
 * @returns {*|number} : the X position on the canvas.
 */
Circle.prototype.getX = function () {
	return this.xpos;
};

/**
 * This function is used to return the Y position of the Circle.
 * @returns {*|number} : the Y position on the canvas.
 */
Circle.prototype.getY = function () {
	return this.ypos;
};

/**
 * This function is used to return the size of the Circle.
 * @returns {size} : the size of the Circle.
 */
Circle.prototype.getSize = function () {
	return this.size;
};